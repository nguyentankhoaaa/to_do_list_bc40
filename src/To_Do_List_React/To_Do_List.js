import React, { Component } from 'react'
import { ThemeProvider } from 'styled-components'
import { Container } from '../container/Container'
import {connect} from "react-redux"
import { Dropdown } from '../container/Dropdown'
import { arrTheme } from '../theme/ThemeManager'
import { Heading3, Heading4, Heading5 } from '../container/Heading'
import { Label, TextField } from '../container/TextField'
import { Button } from '../container/Button'
import { Table, Tbody, Thead, Tr,Th } from '../container/Table'
import { Addtask ,ChangeTheme,CheckTask, DeleteTask, EditTask} from '../Redux/action/action'



 class To_Do_List extends Component {

state={
    taskName : "",
}

renderOptionTask = ()=>{
    return arrTheme.map((task)=>{
        return  <option value={task.id} >{task.name}</option>
    })
}
renderTaskToDo = () =>{
    return this.props.taskList.filter(task => !task.done).map(task=>
       <Tr>
        <Th  style={{verticalAlign:"middle"}}>{task.taskName}</Th>
        <Th className='text-right' >
        <Button className='mx-1' onClick={()=>{
            this.props.dispatch(EditTask(task))
        }} ><i className='fa fa-edit'></i> </Button>
        <Button className='mx-1' onClick={()=>{
          this.props.dispatch(CheckTask(task.id))
        }}   ><i className='fa fa-check'></i> </Button>
        <Button className='mx-1' onClick={()=>{
            this.props.dispatch(DeleteTask(task.id))
        }}><i className='fa fa-trash'></i> </Button>
        </Th>
       </Tr>
        )
}
rendertaskCompleted = ()=>{
    return this.props.taskList.filter(task => task.done).map(task=>
        <Tr>
         <Th  style={{verticalAlign:"middle"}}>{task.taskName}</Th>
         <Th className='text-right' >
         <Button className='mx-1' onClick={()=>{
            this.props.dispatch(DeleteTask(task.id))
         }}><i className='fa fa-trash'></i> </Button>
         </Th>
        </Tr>
         )
}



  render() {
    return (
      <div>
  <ThemeProvider theme={this.props.toDoListTheme}  >
  <Container className='w-50'>
    <Dropdown  onClick={(e)=>{
        let {value} = e.target
        this.props.dispatch(ChangeTheme(value))
    }} >
 { this.renderOptionTask ()}
    </Dropdown>
    <Heading3>To Do List</Heading3>
 <TextField  label="Task Name"  value={this.props.editTask.taskName}  onChange={(e)=>{
    this.setState({
   taskName:e.target.value        
    })      }} >
 </TextField>
 <Button className='mx-2'   onClick={()=>{
    let {taskName} = this.state;
    let task ={
        id: Date.now(),
        taskName:taskName,
        done:false,
    }

    this.props.dispatch(Addtask(task))
 }} ><i class="fa fa-plus "></i> Add Task</Button>

 <Button className='mx-2'><i class="fa fa-upload "></i> Update Task</Button>
 <hr style={{backgroundColor:"white"}} />

 <Heading3>Task To Do</Heading3>
<Table>
    <Thead>
   {this.renderTaskToDo()}
    </Thead>

</Table>
<hr style={{backgroundColor:"white"}} />
<Heading3>Task Completed</Heading3>
<Table>
    <Thead>
     {this.rendertaskCompleted()}
    </Thead>
</Table>



 </Container>
  </ThemeProvider>
      </div>
    )
  }
}

const mapStateToProps = (state)=>{
return {
    toDoListTheme:state.ToDoListReducer.toDoListTheme,
    taskList : state.ToDoListReducer.taskList,
     editTask : state.ToDoListReducer.editTask,
}
}
export default connect(mapStateToProps)(To_Do_List)