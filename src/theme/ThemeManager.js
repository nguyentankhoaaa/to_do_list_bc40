import { toDoListDarktheme } from "./ToDoListDarkTheme";
import { toDoListLighttheme } from "./ToDoListLightTheme";
import { toDoListPrimnarytheme } from "./ToDoListPrimaryTheme";

export const arrTheme = [
{
    id:1,
    name:"Dark-Theme",
    theme:toDoListDarktheme,
},
{
    id:2,
    name:"Light-Theme",
    theme:toDoListLighttheme,
},{
    id:3,
    name:"primary-Theme",
    theme:toDoListPrimnarytheme,
}

]