import { ADD_TASK, CHANGE_THEME, CHECK_TASK, DELETE_TASK, EDIT_TASK } from "../constants/constant";

export const Addtask = (task) => ({
  type: ADD_TASK,
  payload:task,
})

export const CheckTask = (id) => ({
  type: CHECK_TASK,
  payload:id
})


export const DeleteTask = (id) => ({
type: DELETE_TASK,
  payload:id,
})

export const ChangeTheme = (id) => ({
  type: CHANGE_THEME,
  payload:id,
})


export const EditTask = (task) => ({
  type:EDIT_TASK,
  payload:task
})
