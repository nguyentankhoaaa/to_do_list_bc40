import { arrTheme } from "../../theme/ThemeManager";
import { toDoListDarktheme } from "../../theme/ToDoListDarkTheme"
import { ADD_TASK, CHANGE_THEME, CHECK_TASK, DELETE_TASK, EDIT_TASK } from "../constants/constant"

const initialState = {
    toDoListTheme : toDoListDarktheme,
    taskList :[
      {
      id:"task-1",  taskName:"Task 1",  done:true,
    },
    {
      id:"task-2",  taskName:"Task 2",  done:false,
    },
    {
      id:"task-3",  taskName:"Task 3",  done:true,
    },
    {
      id:"task-4",  taskName:"Task 4",  done:false,
    },
  ],
editTask: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
  case ADD_TASK :{
    let newTask = [...state.taskList];
    let index = state.taskList.findIndex(task=>task.taskName == action.payload.taskName);
    if(action.payload.taskName.trim()==""){
      alert("Task Name is not required !");
      return {...state}
    }
    if(index != -1){
      alert("  Task name already exist !");
     return {...state}
     }

    newTask.push(action.payload)

     return{...state,taskList:newTask}
  }
  case CHECK_TASK:{
  let newtask = [...state.taskList];
  let index= newtask.findIndex(task => task.id == action.payload)
   newtask[index].done = true
   return {...state,taskList:newtask}
  }
  case DELETE_TASK :{
    let newtask= state.taskList.filter(task => task.id != action.payload)
    return {...state,taskList:newtask}
  }
  case CHANGE_THEME:{
  
    let index =arrTheme.findIndex(theme=>theme.id == action.payload);
    if(index != -1){
      state.toDoListTheme =arrTheme[index].theme
    }
    return{...state}
  }

  case EDIT_TASK:{
 
   
    state.editTask=action.payload
  
     return {...state}
  }




  default:
    return state
  }
}
